# -*- coding: utf-8 -*-
"""
Created on Tue Jun  1 10:31:22 2021

@author: caponn
"""

import imageio
import matplotlib.pyplot as plt
from skimage import color, io, filters
from scipy import ndimage
import numpy as np
from PIL import Image
from scipy.interpolate import interp1d

k=0
if (k==0): f = imageio.imread('Data/cam2 UTC 19-07-07_08-59-59-46.jpg')
if (k==1): f = imageio.imread('Data/cam2 UTC 19-07-01_18-59-59-43.jpg')
if (k==2): f = imageio.imread('Data/cam2 UTC 19-07-02_16-59-59-51.jpg')
if (k==3): f = imageio.imread('Data/cam2 UTC 19-07-03_04-59-59-59.jpg')
if (k==4): f = imageio.imread('Data/cam2 UTC 19-07-06_08-59-59-56.jpg')

f_gray=color.rgb2gray(f)
hauteur,largeur=f_gray.shape

def Mask(Test2):
    Mask = imageio.imread('masque.png')
    Mask = Mask[:,:,0]
    
    Masque=np.copy(Test2)
    
    for i in range(3):
        b = Masque[:,:,i]
        b[Mask==0] = 0
        a=sum((Mask!=0).sum(0))
        Masque[:,:,i] = b
    return [Masque,a]

def Sobel_Detection(Test):
    Test_gray = color.rgb2gray(Test)
    edge_sobel = filters.sobel(Test_gray)
#    plt.imshow(edge_sobel, cmap=plt.cm.gray)
#    plt.title("Sobel Edge Detection")
#    plt.show()
    return edge_sobel

def Contrast_nuage(f):
    n=0
    for i in range(hauteur):
        for j in range(largeur):
            a=np.mean(f[i,j])
            if (a>=int(0.6*256)) and (a<=int(0.65*255)):
                f[i,j]=[255,0,0]
                n+=1
            else:
                f[i,j]=[0,0,0]
    return [f,n]

def Detection_nuage(f):
    res=np.copy(f)
    s=Sobel_Detection(Contrast_nuage(f)[0])
    for i in range(hauteur):
        for j in range(largeur):
            if (s[i,j]!=0):
                res[i,j]=[125,0,0]
    return res

def couverture_nuage(f):
    a=Mask(f)[1]
    b=Contrast_nuage(m)[1]
    return b/a

[m,a]=Mask(f)
plt.imshow(m)
plt.show()
plt.imshow(Detection_nuage(m))
plt.show()
#print(couverture_nuage(m))