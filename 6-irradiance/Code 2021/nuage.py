# -*- coding: utf-8 -*-
"""
Created on Thu May 20 09:32:58 2021

@author: ponsortt
"""
import numpy as np
import imageio
Nuages = imageio.imread('Nuages2.jpg')

import matplotlib.pyplot as plt
plt.imshow(Nuages)
plt.show()

from skimage import color, io
from scipy import ndimage

Nuages_gray = color.rgb2gray(Nuages)
plt.imshow(Nuages_gray, cmap=plt.cm.gray)
plt.colorbar()

hauteur,largeur=Nuages_gray.shape

eye=np.copy(Nuages_gray)
for i in range(hauteur):
    for j in range(largeur):
        if (eye[i,j]<=0.15):
            eye[i,j]=0
        else:
            eye[i,j]=1

sun=np.copy(Nuages_gray)
for i in range(hauteur):
    for j in range(largeur):
        if (sun[i,j]<=0.9999):
            sun[i,j]=0
        else :
            sun[i,j]=1
            
sun_inv=np.copy(Nuages_gray)
for i in range(hauteur):
    for j in range(largeur):
        if (sun_inv[i,j]<=0.9999):
            sun_inv[i,j]=1
        else :
            sun_inv[i,j]=0
            
clear_clouds=np.copy(Nuages_gray)
for i in range(hauteur):
    for j in range(largeur):
        if (clear_clouds[i,j]<=0.55):
            clear_clouds[i,j]=0
        elif (sun_inv[i,j]==0) :
            clear_clouds[i,j]=0
        else:
            clear_clouds[i,j]=1
                
dark_clouds=np.copy(Nuages_gray)
for i in range(hauteur):
    for j in range(largeur):
        if (dark_clouds[i,j]<=0.35):
            dark_clouds[i,j]=0
        elif (clear_clouds[i,j]==1):
            dark_clouds[i,j]=0
        elif (sun_inv[i,j]==0) :
            dark_clouds[i,j]=0
        else:
            dark_clouds[i,j]=1

plt.imshow(eye, cmap='gray')
plt.show()

plt.imshow(sun, cmap='gray')
plt.show()

plt.imshow(clear_clouds, cmap='gray')
plt.show()

plt.imshow(dark_clouds, cmap='gray')
plt.show()





import numpy as np
from skimage import filters

edge_eye_sobel = filters.sobel(eye)
edge_sun_sobel = filters.sobel(sun)
edge_clear_clouds_sobel = filters.sobel(clear_clouds)
edge_dark_clouds_sobel = filters.sobel(dark_clouds)

"""
fig, axes = plt.subplots(ncols=2, sharex=True, sharey=True,
                         figsize=(16, 8))

axes[1].imshow(edge_eye_sobel, cmap=plt.cm.gray)
axes[1].set_title('Sobel Edge Detection')


for ax in axes:
    ax.axis('off')

plt.tight_layout()
plt.show()

fig, axes = plt.subplots(ncols=2, sharex=True, sharey=True,
                         figsize=(16, 8))

axes[1].imshow(edge_sun_sobel, cmap=plt.cm.gray)
axes[1].set_title('Sobel Edge Detection')

for ax in axes:
    ax.axis('off')

plt.tight_layout()
plt.show()

fig, axes = plt.subplots(ncols=2, sharex=True, sharey=True,
                         figsize=(16, 8))

axes[1].imshow(edge_clear_clouds_sobel, cmap=plt.cm.gray)
axes[1].set_title('Sobel Edge Detection')

for ax in axes:
    ax.axis('off')

plt.tight_layout()
plt.show()

fig, axes = plt.subplots(ncols=2, sharex=True, sharey=True,
                         figsize=(16, 8))

axes[1].imshow(edge_dark_clouds_sobel, cmap=plt.cm.gray)
axes[1].set_title('Sobel Edge Detection')

for ax in axes:
    ax.axis('off')

plt.tight_layout()
plt.show()
"""



resultat=np.copy(Nuages)
for i in range (largeur):
    for j in range(hauteur):
        if (j>140) or (i>400):
            if (edge_eye_sobel[j,i]!=0):
                resultat[j,i]=[0,0,255]
            elif (edge_sun_sobel[j,i]!=0):
                resultat[j,i]=[255,0,0]
            elif (edge_clear_clouds_sobel[j,i]!=0):
                resultat[j,i]=[0,255,0]
            elif (edge_dark_clouds_sobel[j,i]!=0):
                resultat[j,i]=[0,125,0]
            else:
                #resultat[j,i]=Test2[i,j]
                resultat[j,i]=[0,0,0]
        else:
            resultat[j,i]=[0,0,0]

plt.imshow(resultat)
print(largeur, hauteur)
plt.show()

plt.imshow(Nuages)
plt.show()