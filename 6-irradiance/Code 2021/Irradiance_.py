
import imageio
f = imageio.imread('cam2_UTC_19-07-01_11-59-59-46.jpg')


# In[24]:


import matplotlib.pyplot as plt
plt.imshow(f)
plt.show()


# In[9]:


print((f.shape, f.min(), f.max(), f.dtype))
print(f.mean())



# In[46]:


from skimage import color, io
import matplotlib.pyplot as plt
from scipy import ndimage
import numpy as np

g = color.rgb2gray(f)
plt.imshow(g, cmap=plt.cm.gray)
plt.colorbar()


# In[49]:


import numpy as np
import matplotlib.pyplot as plt

from skimage import filters


image = g
edge_roberts = filters.roberts(image)
edge_sobel = filters.sobel(image)

fig, axes = plt.subplots(ncols=2, sharex=True, sharey=True,
                         figsize=(16, 8))

axes[0].imshow(edge_roberts, cmap=plt.cm.gray)
axes[0].set_title('Roberts Edge Detection')

axes[1].imshow(edge_sobel, cmap=plt.cm.gray)
axes[1].set_title('Sobel Edge Detection')

for ax in axes:
    ax.axis('off')

plt.tight_layout()
plt.show()


# In[79]:


import imageio
Test2 = imageio.imread('cam2_UTC_19-07-01_18-59-59-43.jpg')

import matplotlib.pyplot as plt
plt.imshow(Test2)
plt.show()

from skimage import color, io
from scipy import ndimage

Test2_gray = color.rgb2gray(Test2)
plt.imshow(Test2_gray, cmap=plt.cm.gray)
plt.colorbar()

import numpy as np
from skimage import filters

image = Test2_gray
edge_roberts = filters.roberts(image)
edge_sobel = filters.sobel(image)

fig, axes = plt.subplots(ncols=2, sharex=True, sharey=True,
                         figsize=(16, 8))

axes[0].imshow(edge_roberts, cmap=plt.cm.gray)
axes[0].set_title('Roberts Edge Detection')

axes[1].imshow(edge_sobel, cmap=plt.cm.gray)
axes[1].set_title('Sobel Edge Detection')

for ax in axes:
    ax.axis('off')

plt.tight_layout()
plt.show()


# In[80]:


hauteur,largeur=Test2_gray.shape
sun=np.copy(Test2_gray)
for i in range(hauteur):
    for j in range(largeur):
        if (sun[i,j]<=0.9999):
            sun[i,j]=0
        else:
            sun[i,j]=1

eye=np.copy(Test2_gray)
for i in range(hauteur):
    for j in range(largeur):
        if (eye[i,j]<=0.15):
            eye[i,j]=0
        else:
            eye[i,j]=1

plt.imshow(Test2_gray, cmap='gray')
plt.show()

plt.imshow(sun, cmap='gray')
plt.show()

plt.imshow(eye, cmap='gray')
plt.show()


# In[84]:


image = sun
edge_sun_roberts = filters.roberts(image)
edge_sun_sobel = filters.sobel(image)

fig, axes = plt.subplots(ncols=2, sharex=True, sharey=True,
                         figsize=(16, 8))

axes[0].imshow(edge_sun_roberts, cmap=plt.cm.gray)
axes[0].set_title('Roberts Edge Detection')

axes[1].imshow(edge_sun_sobel, cmap=plt.cm.gray)
axes[1].set_title('Sobel Edge Detection')

for ax in axes:
    ax.axis('off')


plt.tight_layout()
plt.show()


# In[85]:


image = eye
edge_eye_roberts = filters.roberts(image)
edge_eye_sobel = filters.sobel(image)

fig, axes = plt.subplots(ncols=2, sharex=True, sharey=True,
                         figsize=(16, 8))

axes[0].imshow(edge_eye_roberts, cmap=plt.cm.gray)
axes[0].set_title('Roberts Edge Detection')

axes[1].imshow(edge_eye_sobel, cmap=plt.cm.gray)
axes[1].set_title('Sobel Edge Detection')

for ax in axes:
    ax.axis('off')

plt.tight_layout()
plt.show()


# In[90]:

resultat=np.copy(Test2)
for i in range (largeur):
    for j in range(hauteur):
        if (j>140) or (i>400):
            if (edge_eye_sobel[j,i]!=0):
                resultat[j,i]=[0,0,255]
            elif (edge_sun_sobel[j,i]!=0):
                resultat[j,i]=[255,0,0]
            else:
                #resultat[j,i]=Test2[i,j]
                resultat[j,i]=[0,0,0]
        else:
            resultat[j,i]=[0,0,0]

plt.imshow(resultat)
print(largeur, hauteur)
plt.show()


# In[ ]:





# In[ ]:



