# -*- coding: utf-8 -*-
"""
Created on Mon May 31 12:08:26 2021

@author: PONSORT
"""

import csv

Tableau = [] #Tableau est une liste de listes (dates, angles, irradiance, ...)
TS = []
Angle = []
Irradiance = []

# lecture du fichier csv
f = open('Meteo_GreEnER_2_01_Hour_2019.csv')
csv_f = csv.reader(f)

for ligne in csv_f:
  Tableau.append(ligne)
  
f.close
  
n = len(Tableau)
for i in range(4,n):
  TS.append(Tableau[i][0])
  Angle.append(Tableau[i][9])
  Irradiance.append(Tableau[i][12])


def RechercheAngleIrradiance(date_image) :
  date = '20' + date_image[-24] + date_image[-23] + '-' + date_image[-21] + date_image[-20] + '-' + date_image[-18] + date_image[-17] + ' ' + date_image[-15] + date_image[-14] + ':' + date_image[-12] + date_image[-11] + ':' + date_image[-9] + date_image[-8]
  indice = 0
  angle = 0
  irradiance = 0
  heure_image = date[-8]+date[-7]
  jour_image = date[-11]+date[-10]
  mois_image = date[-14]+date[-13]
  heure_suivante = 0
  jour_suivant = 0
  mois_suivant = 0
  if(date[-13]=="5"):
    heure_suivante = 1
  if(heure_image + ":" + date[-5] == "23:5"):
    jour_suivant = 1
    heure_image = 0
    heure_suivante = 0
  if( (mois_image == '04' or mois_image == '06' or mois_image == '09' or mois_image == '11' )and (jour_image == '30') and (heure_image + ":" + date[-5] == "23:5") ):
    mois_suivant = 1
    heure_image = 0
    heure_suivante = 0
  elif( (mois_image == '02') and (jour_image== "28") and (heure_image + ":" + date[-5] == "23:5") ):
    mois_suivant = 1
    heure_image = 0
    jour_suivant = 0
    heure_suivante = 0
  elif(jour_image == "31" and (heure_image + ":" + date[-5] == "23:5")) :
    mois_suivant = 1
    heure_image = 0
    jour_image = 1
    jour_suivant = 0
    heure_suivante = 0
  heure_valeur = int(heure_image) + heure_suivante
  jour_valeur = int(jour_image) + jour_suivant
  mois_valeur = int(mois_image) + mois_suivant
  for element in TS :
    if (heure_valeur == int(element[-8] + element[-7])) and (jour_valeur == int(element[-11]+element[-10])) and (mois_valeur == int(element[-14] + element[-13])) :
      indice = TS.index(element)
      angle = Angle[indice+1]
      irradiance = Irradiance[indice+1]
  return date, angle, irradiance

print(RechercheAngleIrradiance("cam2 UTC 19-07-01_12-59-59-46.jpg"))