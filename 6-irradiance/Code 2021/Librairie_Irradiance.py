# -*- coding: utf-8 -*-
"""
Created on Sat May 22 15:11:27 2021

@author: caponn
"""

import imageio
import matplotlib.pyplot as plt
from skimage import color, io, filters
from scipy import ndimage
import numpy as np
from PIL import Image
from scipy.interpolate import interp1d

f = imageio.imread('Data/cam2 UTC 19-07-05_15-59-59-37.jpg')
g = imageio.imread('Data/cam2 UTC 19-07-07_17-59-59-50.jpg')

f_gray=color.rgb2gray(f)
hauteur,largeur=f_gray.shape
slope,intercept=0.09907717016267961,-6.666339309727121
S,I=-14.54595443148215,1276.293307615757

def Mask(Test2):
    Mask = imageio.imread('masque.png')
    Mask = Mask[:,:,0]
    
    Masque=np.copy(Test2)
    
    for i in range(3):
        b = Masque[:,:,i]
        b[Mask==0] = 0
        Masque[:,:,i] = b
    return Masque

def Sobel_Detection(Test):
    Test_gray = color.rgb2gray(Test)
    edge_sobel = filters.sobel(Test_gray)
#    plt.imshow(edge_sobel, cmap=plt.cm.gray)
#    plt.title("Sobel Edge Detection")
#    plt.show()
    return edge_sobel


def Roberts_Detection(Test):
    Test_gray = color.rgb2gray(Test)
    edge_roberts = filters.roberts(Test_gray)
#    plt.imshow(edge_roberts, cmap=plt.cm.gray)
#    plt.title("Roberts Edge Detection")
#    plt.show()
    return edge_roberts

def PreDetection_soleil(f):
    f_gray=color.rgb2gray(f)
    sun=np.copy(f_gray)
    for i in range(hauteur):
        for j in range(largeur):
            if (sun[i,j]<=0.9999):
                sun[i,j]=0
            else:
                sun[i,j]=1
#    plt.imshow(sun, cmap='gray')
#    plt.title("Pré détection du soleil (sun)")
#    plt.show()
    return sun

def Detection_soleil(f):
    sun=PreDetection_soleil(f)
    edge_sun_sobel=Sobel_Detection(sun)
    
    resultat=np.copy(f)
    c=Detection_centreImage(f)
    s=Detection_centreSoleil(f)
    for i in range (largeur):
        for j in range(hauteur):
            if (edge_sun_sobel[j,i]!=0):
                resultat[j,i]=[255,0,0]
            elif (np.absolute(j-c[0])<=10) and (np.absolute(i-c[1])<=10):
                resultat[j,i]=[0,255,0]
            elif (np.absolute(j -s[0])<=10) and (np.absolute(i-s[1])<=10):
                resultat[j,i]=[0,0,255]
            else:
                resultat[j,i]=f[j,i]
    plt.imshow(resultat)
    plt.title("Contour de détection")
    plt.show()
    return sun

def Detection_centreImage(f):
    return [hauteur//2,largeur//2]

def Detection_centreSoleil(f):
    mask=Mask(f)
    sun=PreDetection_soleil(mask)
    a,r,D,l,h,d,n=0,0,0,0,0,0,0
    for i in range(largeur):
        for j in range (hauteur):
            if (sun[j,i]!=0):
                if a==0: d=j
                if n==0: n=1
                a+=1
        if (a>r):
            r=a
            h=d+a//2
        a=0
    for i in range(largeur):
        if (sun[h,i]!=0):
                if a==0: D=i
                a+=1
    l=D+a//2
    if n!=0:
        return [h,l]
    else:
        return [0,l//2]

def Angle_Soleil(f):
    fo=1.6
    ro=1335.886
    s=Detection_centreSoleil(f)
    c=Detection_centreImage(f)
    x=np.absolute(c[0]-s[0])
    y=np.absolute(c[1]-s[1])
    r=np.sqrt(x**2+y**2)/ro
    alpha=2*np.arcsin(r/fo)
    return alpha

def Nuance(f):
    res=np.copy(f)
    for i in range(largeur):
        for j in range (hauteur):
            res[j][i]=np.sqrt((f[j][i][0]**2+f[j][i][1]**2+f[j][i][2]**2)/3)
    plt.imshow(res)
    plt.show()

def predict(x):
   return slope * x + intercept

def Angle_Soleil2(f):
    s=Detection_centreSoleil(f)
    c=Detection_centreImage(f)
    x=np.absolute(c[0]-s[0])
    y=np.absolute(c[1]-s[1])
    r=np.sqrt(x**2+y**2)
    return predict(r)

def calcul_irradiance(f):
    alpha=Angle_Soleil2(f)
    return S*alpha+I

def classification(f):
    res=np.copy(f)
    for i in range(largeur):
        for j in range (hauteur):
            if (f[j][i][2]+f[j][i][0]!=0):
#                print((f[j][i][0],f[j][i][2]),np.absolute(f[j][i][0]-f[j][i][2])//(f[j][i][0]+f[j][i][2])*255)
#                print((f[j][i][0],f[j][i][2]),np.absolute(f[j][i][0]-f[j][i][2]))
                res[j][i]=np.absolute(f[j][i][0]-f[j][i][2])//(f[j][i][0]+f[j][i][2])*255
    plt.imshow(res)
    plt.show()
    

mask=Mask(g)
m=Mask(f)
plt.imshow(mask)
plt.show()
Detection_soleil(mask)
#print(calcul_irradiance(mask))

# =============================================================================
# DEtermination des fonctions liants angle(rayon) et irradiance(angle)
# =============================================================================

#M=np.zeros([10,2])
#for k in range(10):
#    z=0
#    if k==0 : A,z,i=Mask(io.imread('Data/cam2 UTC 19-07-01_04-59-59-31.jpg')),85,19.31
#    if k==1 : A,z,i=Mask(io.imread('Data/cam2 UTC 19-07-01_05-59-59-29.jpg')),75.52,129.4
#    if k==2 : A,z,i=Mask(io.imread('Data/cam2 UTC 19-07-01_06-59-59-27.jpg')),65.3,345.2
#    if k==3 : A,z,i=Mask(io.imread('Data/cam2 UTC 19-07-01_07-59-59-35.jpg')),54.8,517.4
#    if k==4 : A,z,i=Mask(io.imread('Data/cam2 UTC 19-07-01_08-59-59-39.jpg')),44.36,673.8
#    if k==5 : A,z,i=Mask(io.imread('Data/cam2 UTC 19-07-01_09-59-59-38.jpg')),34.56,798.1
#    if k==6 : A,z,i=Mask(io.imread('Data/cam2 UTC 19-07-01_10-59-59-40.jpg')),26.54,879
#    if k==7 : A,z,i=Mask(io.imread('Data/cam2 UTC 19-07-01_11-59-59-46.jpg')),22.47,907
#    if k==8 : A,z,i=Mask(io.imread('Data/cam2 UTC 19-07-01_12-59-59-46.jpg')),24.48,897
#    if k==9 : A,z,i=Mask(io.imread('Data/cam2 UTC 19-07-01_13-59-59-30.jpg')),31.41,841
#    m=Mask(A)
##    c=Detection_centreImage(A)
##    s=Detection_centreSoleil(A)
##    x=np.absolute(c[0]-s[0])
##    y=np.absolute(c[1]-s[1])
##    r=np.sqrt(x**2+y**2)
#    r=Angle_Soleil2(m)
#    M[k,:]=[i,r]   #mettre z pour angle ou i pour irradiance
#
#from scipy import stats
#
#slope, intercept, r_value, p_value, std_err = stats.linregress(M[:,1],M[:,0])
#
#print(slope,intercept)
#
#f = interp1d(M[:,1],M[:,0])
#f2 = interp1d(M[:,1],M[:,0], kind='cubic')
#xnew = np.linspace(min(M[:,1]), max(M[:,1]), num=41, endpoint=True)
#
#plt.plot(M[:,1],M[:,0], 'o', xnew, f(xnew), 'm-', xnew, f2(xnew), 'g--')
#fitLine = predict(M[:,1])
#plt.plot(M[:,1], fitLine,'r-',label='Regres Lin')
#plt.legend(['data', 'Interpolation linéaire', 'Interpolation polynomiale','Regression Linéaire'], loc='best')
#
#plt.show()

