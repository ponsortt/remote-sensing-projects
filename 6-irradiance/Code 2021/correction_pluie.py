# -*- coding: utf-8 -*-
"""
Created on Tue Jun  1 10:25:41 2021

@author: caponn
"""

import imageio
import matplotlib.pyplot as plt
from skimage import color, io, filters
from scipy import ndimage
import numpy as np
from PIL import Image
from scipy.interpolate import interp1d

#f = imageio.imread('Data/cam2 UTC 19-07-05_15-59-59-37.jpg')

k=3
if (k==1): f = imageio.imread('Pluie/test_pluie_1.jpg')
if (k==2): f = imageio.imread('Pluie/test_pluie_2.png')
if (k==3): f = imageio.imread('Pluie/test_pluie_3.jpg')
if (k==4): f = imageio.imread('Pluie/test_pluie_4.jpg')

f_gray=color.rgb2gray(f)
hauteur,largeur=f_gray.shape

def Mask(Test2):
  
    Mask = imageio.imread('masque.png')
    Mask = Mask[:,:,0]
    
    Masque=np.copy(Test2)
    
    for i in range(3):
        b = Masque[:,:,i]
        b[Mask==0] = 0
        Masque[:,:,i] = b
    return Masque


def Detection_goutte(f):
    res=np.copy(f_gray)
    for i in range(hauteur):
        for j in range(largeur):
            if (np.average(f[i,j])/255<0.3): res[i,j]=0
            else: res[i,j]=1
    return res
    
def Correction_pluie(f):
    r=Detection_goutte(f)
    for i in range(hauteur):
        for j in range(1,largeur-1):
            if r[i,j]==0: #and (j+50<largeur-1): 
#                a,c,d,n=f[i,j-50],1,2,1
##                while ((c!=0) or (d!=0)) and (j+n<largeur-1):
##                    if (r[i,j+n]!=0): c=0
##                    if (c==0) and (r[i,j+n]==0): d=1
##                    if (d==1) and (r[i,j+n]!=0): d=0
##                    n+=1
#                while ((r[i,j+n]==0) and (j+n+50<largeur-1)):
##                    f[i,j+n]=a
#                    n+=1
##                print(j+n+5)
#                b,v=f[i,j+n+50],[]
#                for m in range(3):
#                    v.append((a[m]+b[m])//2)
##                    v.append(np.linspace(a[m],b[m],n))
#                for m in range(n):
#                    f[i,j+m]=v
##                    f[i,j+m]=[int(v[0][m]),int(v[1][m]),int(v[2][m])]
#                j=j+n+50-1
                k,n=f[i,j-5],1
                f[i,j]=k
                while ((r[i,j+n]==0) and (j+n<largeur-1)):
                    f[i,j+n]=k
                    n+=1
#                print(j+n)
                j=j+n
                
    return f 

#m=Mask(f)
plt.imshow(Detection_goutte(f), cmap='gray')
plt.show()
plt.imshow(f)
plt.show()
plt.imshow(Correction_pluie(f))
plt.show()