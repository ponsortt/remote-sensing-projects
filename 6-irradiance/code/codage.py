# -*- coding: utf-8 -*-
"""
Created on Mon May  4 08:29:20 2020

@author: comed
"""


from imageio import *
import matplotlib.pyplot as plt
import numpy as np
from math import *


h=5000 #hauteur moyenne nuage
rayon=750 
im=imread('echantillon_30_07.jpg')

imax=len(im)
print (imax)
ic=imax//2

jmax=len(im[0])
print (jmax)
jc=jmax//2
for i in range (imax):
    for j in range (jmax):
        if (((i-ic)**2+(j-jc)**2)**0.5>rayon):
            im[i][j]=(255,0,0)
plt.imshow(im)
plt.show()


