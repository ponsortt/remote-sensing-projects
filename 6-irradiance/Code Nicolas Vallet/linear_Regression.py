'''
Created on 22 mai 2020

@author: Nicolas Vallet
'''

import numpy as np
from sklearn.linear_model import LinearRegression
import glob
import tensorflow as tf
import numpy as np
from tensorflow import keras
from tensorflow.keras import layers
import matplotlib.pyplot as plt
import cv2
import pandas as pd
from PIL import Image
from sklearn.linear_model import LinearRegression

AUTOTUNE = tf.data.experimental.AUTOTUNE

# Calcul du coefficient de Nash
def Nash_Sutcliffe_coefficient(obsData, simData):
    E = np.subtract(obsData,simData)
    E = E[~np.isnan(E)]
    E = [data**2 for data in E] 
    SSE = sum(E)
    obsData = np.array(obsData)
    obsData = obsData[~np.isnan(obsData)]
    u = np.mean(obsData)
    SSU = [(data-u) for data in obsData]
    SSU = [data**2 for data in SSU]
    SSU = sum(SSU)
    NSout = 1 - SSE/SSU
    return NSout

# LINEAR REGRESSION 

load_base = np.load("Base_entrainement.npz")
training_values = np.array([load_base["training_values"]])
training_values = training_values.transpose()
training_images = load_base["training_images"]
training_images = np.reshape(training_images, [-1, 498000, 1, 1])
training_images = training_images[:, :, 0, 0]

LINEAR_REGRESSION = LinearRegression().fit(training_images, training_values)

print(LINEAR_REGRESSION.coef_)

# Calcul du Coefficient de Nash

observed_values = [data[0] for data in training_values]
simulated_values = LINEAR_REGRESSION.predict(training_images)
simulated_values = [ data[0] for data in simulated_values ]

NS = Nash_Sutcliffe_coefficient(simulated_values, observed_values)
print(NS)