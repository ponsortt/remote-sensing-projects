'''
Created on 15 avr. 2020

@author: Nicolas Vallet
'''

import glob
import tensorflow as tf
import numpy as np
from tensorflow import keras
from tensorflow.keras import layers
import matplotlib.pyplot as plt
import cv2
import pandas as pd
from PIL import Image
import tensorflow_docs as tfdocs
import tensorflow_docs.plots
import tensorflow_docs.modeling

AUTOTUNE = tf.data.experimental.AUTOTUNE

def build_model_CNN():
    model = keras.Sequential()
    model.add(layers.Conv2D(3,(3, 3), activation='relu', input_shape=(415, 400, 3)))
    model.add(layers.MaxPooling2D((2, 2)))
    model.add(layers.Conv2D(3, (3, 3), activation='relu'))
    model.add(layers.MaxPooling2D((2, 2)))
    model.add(layers.Conv2D(3, (3, 3), activation='relu'))
    model.add(layers.MaxPooling2D((2, 2)))
    model.add(layers.Conv2D(3, (3, 3), activation='relu'))
    model.add(layers.MaxPooling2D((2, 2)))
    model.add(layers.Conv2D(3, (3, 3), activation='relu'))
    model.add(layers.Flatten())
    model.add(layers.Dense(3, activation='relu'))
    model.add(layers.Dense(3, activation='relu'))
    model.add(layers.Dense(1))
    optimizer = tf.keras.optimizers.RMSprop(0.001)
    model.compile(loss='mse',optimizer=optimizer,metrics=['mae', 'mse'])
    return model

#Reduce the size of the image according to the ratio
def Preprocess_image(image_path,ratio):
    image = tf.io.read_file(image_path)
    crop_y = int(140/ratio)
    crop_x = int(535/ratio)
    crop_height = int(1660/ratio)
    crop_width = int(1602/ratio)
    crop_window=[crop_y, crop_x, crop_height, crop_width]
    img = tf.io.decode_and_crop_jpeg(image,crop_window,channels=0, ratio=ratio)
    img=img/255
    return img
  
def Training_Data_Base_Creation(annee, nmr_mois, jour_depart, nbre_jour_base, ratio_cprn):
    training_images_tmp = []
    training_values_tmp = []
    nmr_annee = str(annee)
    df = pd.read_csv("Cam2/2019/Mesure2019/Meteo_GreEnER_01_Hour_"+nmr_annee+".txt",skiprows = lambda x : x==0 or x == 2 or x== 3 )
    jours_en_cours = jour_depart
    while jours_en_cours<(jour_depart+nbre_jour_base):
        nmr_jr=str(jours_en_cours).zfill(2)
        nmr_mois_str=str(nmr_mois).zfill(2)
        if(glob.glob("Cam2/"+nmr_annee+"/"+nmr_annee[2]+nmr_annee[3]+nmr_mois_str+"/cam2 UTC "+nmr_annee[2]+nmr_annee[3]+"-"+nmr_mois_str+"-"+nmr_jr+"_*.jpg")!=[]):
            list_ds = tf.data.Dataset.list_files("Cam2/"+nmr_annee+"/"+nmr_annee[2]+nmr_annee[3]+nmr_mois_str+"/cam2 UTC "+nmr_annee[2]+nmr_annee[3]+"-"+nmr_mois_str+"-"+nmr_jr+"_*.jpg",shuffle=False)
            if (len(str(list_ds)) < 24 ) :
                print(nmr_jr);
            #We take all the image from the given day
            for image_path in list_ds:
                heure_image = int(str(image_path.numpy())[-16]+str(image_path.numpy())[-15])
                jour_image = int(str(image_path.numpy())[-19]+str(image_path.numpy())[-18])
                mois_image = int(str(image_path.numpy())[-22]+str(image_path.numpy())[-21])
                heure_suivante = 0
                jour_suivant = 0
                mois_suivant = 0
                if(str(image_path.numpy())[-13]=="5"):
                    heure_suivante = 1
                if(str(image_path.numpy())[-16]+str(image_path.numpy())[-15]+":"+str(image_path.numpy())[-13] == "23:5"):
                    jour_suivant = 1 
                    heure_image = 0
                    heure_suivante = 0
                if( (nmr_mois == 4 or nmr_mois == 6 or nmr_mois == 9 or nmr_mois == 11 )and (jour_image == 30) and (str(image_path.numpy())[-16]+str(image_path.numpy())[-15]+":"+str(image_path.numpy())[-13] == "23:5") ):
                    mois_suivant = 1
                    heure_image = 0
                    heure_suivante = 0
                elif( (nmr_mois == 2) and (str(image_path.numpy())[-19]+str(image_path.numpy())[-18]== "28") and not(annee%400 == 0 or (annee%4 == 0 and annee%100 != 0)) and (str(image_path.numpy())[-16]+str(image_path.numpy())[-15]+":"+str(image_path.numpy())[-13] == "23:5") ):
                    mois_suivant = 1
                    heure_image = 0
                    jour_suivant = 0
                    heure_suivante = 0
                elif( (nmr_mois == 2) and (str(image_path.numpy())[-19]+str(image_path.numpy())[-18]== "29") and (str(image_path.numpy())[-16]+str(image_path.numpy())[-15]+":"+str(image_path.numpy())[-13] == "23:5") ):
                    mois_suivant = 1
                    heure_image = 0
                    jour_suivant = 0
                    heure_suivante = 0
                elif(str(image_path.numpy())[-19]+str(image_path.numpy())[-18]== "31" and (str(image_path.numpy())[-16]+str(image_path.numpy())[-15]+":"+str(image_path.numpy())[-13] == "23:5")) :
                    mois_suivant = 1
                    heure_image = 0
                    jour_image = 1
                    jour_suivant = 0
                    heure_suivante = 0
                heure_valeur = heure_image+heure_suivante
                jour_valeur = jour_image+jour_suivant
                mois_valeur = mois_image+mois_suivant
                #We look for the value of the direct irradiance associated with the image       
                for line_csv in df.values:
                    if ((line_csv[0][2:14] == str(image_path.numpy())[-25]+str(image_path.numpy())[-24]+"-"+str(mois_valeur).zfill(2)+"-"+str(jour_valeur).zfill(2)+" "+str(heure_valeur).zfill(2)+":") and (line_csv[15]!='NAN' and line_csv[15]!='nan') ):
                        print(str(image_path.numpy())[-25]+str(image_path.numpy())[-24]+"-"+str(image_path.numpy())[-22]+str(image_path.numpy())[-21]+"-"+str(image_path.numpy())[-19]+str(image_path.numpy())[-18]+" "+str(image_path.numpy())[-16]+str(image_path.numpy())[-15]+":"+str(image_path.numpy())[-13]+str(image_path.numpy())[-12])
                        training_values_tmp.append(line_csv[15])
                        #We process the images one by one and we store it into the train_image array
                        img = Preprocess_image(image_path.numpy(),ratio_cprn)
                        training_images_tmp.append(img)
                        print(line_csv[0][2:14])
            jours_en_cours=jours_en_cours+1
        else : 
            print("le jour n�"+nmr_jr+" n'a aucune photo")
            jours_en_cours=jours_en_cours+1
    training_images = np.stack(training_images_tmp,axis=0)
    training_values = np.stack(training_values_tmp,axis=0)
    training_values = training_values.astype(np.float32)
    return(training_images,training_values)

def Concatenation_Data_Base(Data_Base_1_Path,Data_Base_2_Path):
    load_base_1 = np.load(Data_Base_1_Path)
    load_base_2 = np.load(Data_Base_2_Path)
    train_data_1 = load_base_1["training_images"]
    train_data_2 = load_base_2["training_images"]
    train_values_1 = load_base_1["training_values"]
    train_values_2 = load_base_2["training_values"]
    concatenate_load_base_images =np.concatenate((train_data_1,train_data_2),axis=0)
    concatenate_load_base_values =np.concatenate((train_values_1,train_values_2),axis=0)
    return(concatenate_load_base_images,concatenate_load_base_values)

# Creation and save of the DATABASE

print("Cr�ation de la base d'entrainement")
Training_Data_Base = Training_Data_Base_Creation(2018,11,15,2,4)
print(Training_Data_Base[0].shape)
print(Training_Data_Base[1].shape)
print(Training_Data_Base[0].dtype)
print(Training_Data_Base[1].dtype)
print("Enregistrement de la base d'entrainement")
np.savez_compressed('Base_entrainement.npz', training_images = Training_Data_Base[0] , training_values = Training_Data_Base[1])
print("Enregistrement termin�e")


