import tensorflow as tf
import numpy as np
from tensorflow import keras
from tensorflow.keras import layers
import tensorflow_docs as tfdocs
import tensorflow_docs.plots
import tensorflow_docs.modeling

AUTOTUNE = tf.data.experimental.AUTOTUNE

def build_model_CNN():
    model = keras.Sequential()
    model.add(layers.Conv2D(3,(3, 3), activation='relu', input_shape=(415, 400, 3)))
    model.add(layers.MaxPooling2D((2, 2)))
    model.add(layers.Conv2D(3, (3, 3), activation='relu'))
    model.add(layers.MaxPooling2D((2, 2)))
    model.add(layers.Conv2D(3, (3, 3), activation='relu'))
    model.add(layers.MaxPooling2D((2, 2)))
    model.add(layers.Conv2D(3, (3, 3), activation='relu'))
    model.add(layers.MaxPooling2D((2, 2)))
    model.add(layers.Conv2D(3, (3, 3), activation='relu'))
    model.add(layers.Flatten())
    model.add(layers.Dense(3, activation='relu'))
    model.add(layers.Dense(3, activation='relu'))
    model.add(layers.Dense(1))
    optimizer = tf.keras.optimizers.RMSprop(0.001)
    model.compile(loss='mse',optimizer=optimizer,metrics=['mae', 'mse'])
    return model

#Use of the DATABASE to train the Convolutional Neural Network

model = build_model_CNN()                                                                                                                                                                                                                                                                                                                                                                 
load_base = np.load("Base_entrainement.npz")
train_data = load_base["training_images"]
train_labels = load_base["training_values"]
EPOCHS = 3
print("debut entrainement")
history = model.fit(train_data, train_labels,epochs=EPOCHS, validation_split = 0.1,callbacks=[tfdocs.modeling.EpochDots()])
print("fin entrainement")
print(history.history)